<div class="row poppins text-matt p-32">
   <div class="text-center" style="padding-top: 0px;">
        <center>
            <h2>Contact</h2>
            <i>
                <p class="p-8" style="font-size: 18px;">Kontak kami jika kalian membutuhkan informasi</p>
            </i>
        </center>
   </div>
</div>
<div class="row poppins p-32">
    <div class="col-6 p-8">
        <form action="">
            <div class="text-matt">
                <h3 style="margin-top: 0px; margin-bottom: 24px;">Form</h3>
                <div>
                    <label>Name</label><br>
                    <input type="text" class="input" autocomplete="off" placeholder="Name..." required>
                </div>
                <div style="margin-top: 8px;">
                    <label>Email</label><br>
                    <input type="email" class="input" autocomplete="off" placeholder="Email..." required>
                </div>
                <div style="margin-top: 8px;">
                    <label>Subject</label><br>
                    <input type="text" class="input" autocomplete="off" placeholder="Subject..." required>
                </div>
                <div style="margin-top: 8px;">
                    <label>Message</label><br>
                    <textarea class="input" placeholder="Message..." id="" cols="30" rows="5" required></textarea>
                </div>
                <div style="margin-top: 8px">
                    <input type="submit" class="btn" value="Send">
                </div>
            </div>
        </form>
    </div>
    <div class="col-6 p-8">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <h3 style="margin-top: 0px; margin-bottom: 24px;">Share</h3>
                    <div class="col-4">
                        <a target="_blank" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwp.rairizky.my.id%2F">
                            <img id="icons-rounded" src="assets/icons/facebook.png" alt="fb" style="max-width: 48px;">
                        </a>
                    </div>
                    <div class="col-4">
                        <a target="_blank" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwp.rairizky.my.id%2F">
                            <img id="icons-rounded" src="assets/icons/twitter.png" alt="tw" style="max-width: 48px;">
                        </a>
                    </div>
                    <div class="col-4">
                        <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwp.rairizky.my.id%2F">
                            <img id="icons-rounded" src="assets/icons/linkedin.png" alt="li" style="max-width: 48px;">
                        </a>
                    </div>
                </div>
                <div class="row text-matt" style="margin-top: 24px;">
                    <p>Email : mail@smkn99depok.sch.id</p>
                    <p>Phone : 021 222 666</p>
                    <p>Address : Road Unknown, No. 99, Depok</p>
                </div>
                <div id="map" style="margin-top: 8px;"></div>
            </center>
        </div>  
    </div>
</div>
<script>
    mapboxgl.accessToken = 'pk.eyJ1IjoicmFpcml6a3kiLCJhIjoiY2tjMzlyN3JnMmFxdzJzbGdjMHV0emo2cSJ9.Z9mwxCoWSs2WD4VLd7US5A';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        zoom: 15,
        center: [106.83346528040562, -6.356856583214821],
    });
    new mapboxgl.Marker().setLngLat([106.83346528040562, -6.356856583214821]).addTo(map);
</script>
