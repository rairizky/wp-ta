<div class="row poppins text-matt p-32">
   <div class="text-center" style="padding-top: 0px;">
        <center>
            <h2>Facility</h2>
            <i>
                <p class="p-8" style="font-size: 18px;">Fasilitas di SMKN 99 KOTA DEPOK</p>
            </i>
        </center>
   </div>
</div>
<div class="row poppins p-32">
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/facility/computer-lab.jpg" alt="facility">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Computer Lab</h3>
                </div>
            </center>
        </div>         
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/facility/chemical-lab.jpg" alt="chemical">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Chemistry Lab</h3>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/facility/uks.jpg" alt="uks">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">UKS</h3>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/facility/sport-center.jpg" alt="sport-center">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Sport Center</h3>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/facility/library.jpg" alt="library">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Library</h3>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/facility/canteen.jpg" alt="canteen">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Canteen</h3>
                </div>
            </center>
        </div>    
    </div>
</div>