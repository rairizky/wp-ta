<div class="row poppins text-matt p-32">
    <div class="text-center" style="padding-top:0px;">
        <center>
            <h2>Gallery</h2>
            <i>
                <p class="p-8" style="font-size: 18px;">Galeri Event/Pencapaian/Aktivitas di SMKN 99 KOTA DEPOK</p>
            </i>
        </center>
    </div>
</div>
<div class="gallery row p-32"> 
    <div class="gallery column">
        <img src="assets/images/gallery/1.jpg" style="width:100%">
        <img src="assets/images/gallery/5.jpg" style="width:100%">
        <img src="assets/images/gallery/2.jpg" style="width:100%">
        <img src="assets/images/gallery/3.jpg" style="width:100%">
        <img src="assets/images/gallery/7.jpg" style="width:100%">
        <img src="assets/images/gallery/4.jpg" style="width:100%">
        <img src="assets/images/gallery/6.jpg" style="width:100%">
    </div>
    
    <div class="gallery column">
        <img src="assets/images/gallery/2.jpg" style="width:100%">
        <img src="assets/images/gallery/7.jpg" style="width:100%">
        <img src="assets/images/gallery/5.jpg" style="width:100%">
        <img src="assets/images/gallery/3.jpg" style="width:100%">
        <img src="assets/images/gallery/1.jpg" style="width:100%">
        <img src="assets/images/gallery/6.jpg" style="width:100%">
        <img src="assets/images/gallery/4.jpg" style="width:100%">
    </div>

    <div class="gallery column">
        <img src="assets/images/gallery/3.jpg" style="width:100%">
        <img src="assets/images/gallery/2.jpg" style="width:100%">
        <img src="assets/images/gallery/3.jpg" style="width:100%">
        <img src="assets/images/gallery/4.jpg" style="width:100%">
        <img src="assets/images/gallery/5.jpg" style="width:100%">
        <img src="assets/images/gallery/6.jpg" style="width:100%">
        <img src="assets/images/gallery/7.jpg" style="width:100%">
    </div>

    <div class="gallery column">
        <img src="assets/images/gallery/6.jpg" style="width:100%">
        <img src="assets/images/gallery/2.jpg" style="width:100%">
        <img src="assets/images/gallery/7.jpg" style="width:100%">
        <img src="assets/images/gallery/3.jpg" style="width:100%">
        <img src="assets/images/gallery/1.jpg" style="width:100%">
        <img src="assets/images/gallery/4.jpg" style="width:100%">
        <img src="assets/images/gallery/5.jpg" style="width:100%">
    </div>
</div>