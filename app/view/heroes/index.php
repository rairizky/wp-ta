<div class="row poppins text-matt p-32">
   <div class="text-center" style="padding-top: 0px;">
        <center>
            <h2>Teacher</h2>
            <i>
                <p class="p-8" style="font-size: 18px;">Daftar guru di SMKN 99 KOTA DEPOK</p>
            </i>
        </center>
   </div>
</div>
<div class="row poppins p-32">
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/heroes/pian.jpg" alt="ava ml">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Alvian Rasya Purnama</h3>
                    <p class="text-matt" style="margin-top: 0px;">Mathematics</p>
                </div>
            </center>
        </div>         
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/heroes/arra.jpg" alt="ava ml">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Arra Dwi Hartina</h3>
                    <p class="text-matt" style="margin-top: 0px;">Indonesian</p>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/heroes/fauzan.jpg" alt="ava ml">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Fauzan Akbar</h3>
                    <p class="text-matt" style="margin-top: 0px;">Sains</p>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/heroes/jabaik.jpg" alt="ava ml">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Jabaik Beckam Manurung</h3>
                    <p class="text-matt" style="margin-top: 0px;">Art</p>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/heroes/rafli.jpg" alt="ava ml">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Rafli Rai Rizky</h3>
                    <p class="text-matt" style="margin-top: 0px;">Sociology</p>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <div class="bg-circle">
                    <img class="ava-heroes" src="assets/images/heroes/shafa.JPG" alt="ava ml">
                </div>
                <div class="row" style="margin-top: 32px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Shafa Salsabila Febriani</h3>
                    <p class="text-matt" style="margin-top: 0px;">English</p>
                </div>
            </center>
        </div>    
    </div>
</div>