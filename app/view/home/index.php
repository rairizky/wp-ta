<div class="row">
    <div class="col-4 p-32">
        <img src="assets/images/study.png" style="max-width: 100%;">
    </div>
    <div class="col-8">
        <div class="text-center">
            <h1 style="font-family: 'Poppins'; font-size: 48px; color: #00BFA6; margin-bottom: 0px">Sekolah Menengah Kejuruan</h1>
            <h1 style="font-family: 'Poppins'; font-size: 48px; color: #3d3d3d; margin-top: 0px">Negeri 99 Kota Depok</h1>
        </div>
    </div>
</div>
<div class="row poppins text-matt p-32" style="background-color: #00BFA6; border-radius: 20px;">
    <div class="text-center" style="padding-top: 0px;">
        <center>
            <h2 style="color: #fff;">PPDB SMKN 99 KOTA DEPOK 2021/2022</h2>
            <p style="color: red;">Pendaftaran masih dibuka!</p>
            <p style="color: #fff;">Cara mendaftar sangat mudah, melalui link :</p>
            <p><a target="_blank" href="#">ppdb.smkn99kotadepok.sch.id</a></p>
            <p style="color: #fff;">Kemudian ikuti intruksi hingga selesai.</p>
            <h3 style="color: #fff;">Segera daftar karena kuota <span style="color: red;">TERBATAS!</span></h3>
        </center>
    </div>
</div>
<div class="row poppins text-matt p-32">
   <div class="text-center">
        <center>
            <h2>Motto</h2>
            <i>
                <p class="p-8" style="font-size: 18px;">“Keberhasilan tidak akan tercapai tanpa ilmu pengetahuan. Pendidikan adalah bekal untuk masa depan”</p>
            </i>
        </center>
   </div>
</div>
<div class="row">
    <div class="col-6 p-32">
        <div class="poppins text-center text-matt">
            <h2>Visi</h2>
            <p style="font-size: 18px;">Menjadi sekolah kejuruan teladan nasional yang berbudaya lingkungan, berkarakter kebangsaan, berbasis teknologi informasi dan mampu memenuhi kebutuhan dunia kerja.</p>
        </div>
    </div>
    <div class="col-6 p-32">
        <img src="assets/images/visi.png" style="max-width: 100%;">
    </div>
</div>
<div class="row">
    <div class="col-6 p-32">
        <img src="assets/images/misi.png" style="max-width: 100%;">
    </div>
    <div class="col-6 p-32">
        <div class="poppins text-center text-matt">
            <h2>Misi</h2>
            <ul style="font-size: 18px;">
                <li>Mendidik anak bangsa dengan hati dan teknologi sehingga memenuhi kebutuhan mutu dunia kerja.</li>
                <li>Mewujudkan sekolah sebagai benteng moralitas bangsa.</li>
                <li>Membangun kebersamaan sosial, jiwa kewirusahaan, dan gerakan cinta tanah air dan lingkungan.</li>
            </ul>
        </div>
    </div>
</div>