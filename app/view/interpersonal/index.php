<div class="row poppins text-matt p-32">
   <div class="text-center" style="padding-top: 0px;">
        <center>
            <h2>Interpersonal Skills</h2>
            <i>
                <p class="p-8" style="font-size: 18px;">Daftar ekskul di SMKN 99 KOTA DEPOK</p>
            </i>
        </center>
   </div>
</div>
<div class="row poppins p-32">
    <div class="row">
        <div class="col-4 p-32">
            <div class="text-center" style="padding-top: 0px;">
                <center>
                    <div class="row">
                        <img class="icon-interpersonal" src="assets/images/interpersonal/basketball.png" alt="basketball" style="width: 100px;">
                    </div>
                    <div class="row" style="margin-top: 24px;">
                        <h3 style="color: #00BFA6; margin-bottom: 0px;">Basketball</h3>
                    </div>
                </center>
            </div>    
        </div>
        <div class="col-4 p-32">
            <div class="text-center" style="padding-top: 0px;">
                <center>
                    <div class="row">
                        <img class="icon-interpersonal" src="assets/images/interpersonal/football.png" alt="football" style="width: 100px;">
                    </div>
                    <div class="row" style="margin-top: 24px;">
                        <h3 style="color: #00BFA6; margin-bottom: 0px;">Football</h3>
                    </div>
                </center>
            </div>    
        </div>
        <div class="col-4 p-32">
            <div class="text-center" style="padding-top: 0px;">
                <center>
                    <div class="row">
                        <img class="icon-interpersonal" src="assets/images/interpersonal/badminton.png" alt="badminton" style="width: 100px;">
                    </div>
                    <div class="row" style="margin-top: 24px;">
                        <h3 style="color: #00BFA6; margin-bottom: 0px;">Badminton</h3>
                    </div>
                </center>
            </div>    
        </div>
    </div>
    <div class="row">
        <div class="col-4 p-32">
            <div class="text-center" style="padding-top: 0px;">
                <center>
                    <div class="row">
                        <img class="icon-interpersonal" src="assets/images/interpersonal/chess.png" alt="chess" style="width: 100px;">
                    </div>
                    <div class="row" style="margin-top: 24px;">
                        <h3 style="color: #00BFA6; margin-bottom: 0px;">Chess</h3>
                    </div>
                </center>
            </div>    
        </div>
        <div class="col-4 p-32">
            <div class="text-center" style="padding-top: 0px;">
                <center>
                    <div class="row">
                        <img class="icon-interpersonal" src="assets/images/interpersonal/robot.png" alt="robot" style="width: 100px;">
                    </div>
                    <div class="row" style="margin-top: 24px;">
                        <h3 style="color: #00BFA6; margin-bottom: 0px;">Robotic</h3>
                    </div>
                </center>
            </div>    
        </div>
        <div class="col-4 p-32">
            <div class="text-center" style="padding-top: 0px;">
                <center>
                    <div class="row">
                        <img class="icon-interpersonal" src="assets/images/interpersonal/volleyball.png" alt="volleyball" style="width: 100px;">
                    </div>
                    <div class="row" style="margin-top: 24px;">
                        <h3 style="color: #00BFA6; margin-bottom: 0px;">Volleyball</h3>
                    </div>
                </center>
            </div>    
        </div>
    </div>
</div>