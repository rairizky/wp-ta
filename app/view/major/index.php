<div class="row poppins text-matt p-32">
   <div class="text-center" style="padding-top: 0px;">
        <center>
            <h2>Major</h2>
            <i>
                <p class="p-8" style="font-size: 18px;">Daftar jurusan di SMKN 99 KOTA DEPOK</p>
            </i>
        </center>
   </div>
</div>
<div class="row poppins p-32">
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <img class="icon-major" src="assets/images/major/rpl-2.svg" alt="rpl">
                </div>
                <div class="row" style="margin-top: 24px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Software Engineering</h3>
                    <p class="text-matt">Desktop Programming, Web Programming, Mobile Programming, Bussiness Analyst, Database Administration.</p>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <img class="icon-major" src="assets/images/major/tkj.svg" alt="tkj">
                </div>
                <div class="row" style="margin-top: 24px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Computer and Network Engineering</h3>
                    <p class="text-matt">CNAP (Cisco Networking Academy Program) dan MCNA (Mikrotik Certified Network Associate).</p>
                </div>
            </center>
        </div>    
    </div>
    <div class="col-4 p-32">
        <div class="text-center" style="padding-top: 0px;">
            <center>
                <div class="row">
                    <img class="icon-major" src="assets/images/major/mmd-2.svg" alt="mmd">
                </div>
                <div class="row" style="margin-top: 24px;">
                    <h3 style="color: #00BFA6; margin-bottom: 0px;">Multimedia</h3>
                    <p class="text-matt">Production house, Radio & TV, Photo Studio, Graphic Printing Magazine/Newspaper publisher.</p>
                </div>
            </center>
        </div>    
    </div>
</div>