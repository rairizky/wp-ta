<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SMKN 99 KOTA DEPOK</title>

    <!-- font poppins -->
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <!-- primary app css -->
    <link rel="stylesheet" href="assets/styles/stylesheets/application.css">
    <link rel="stylesheet" href="assets/styles/stylesheets/navbar.css">
    <link rel="stylesheet" href="assets/styles/stylesheets/hero.css">
    <!-- mapbox -->
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
</head>
<body>
    <nav class="navbar">
        <ul>
            <li>
                <a href="?page=home" class="poppins">Home</a>
            </li>
            <li>
                <a href="?page=major" class="poppins">Major</a>
            </li>
            <li>
                <a href="?page=teacher" class="poppins">Teacher</a>
            </li>
            <li>
                <a href="?page=interpersonal" class="poppins">Interpersonal</a>
            </li>
            <li>
                <a href="?page=facility" class="poppins">Facility</a>
            </li>
            <li>
                <a href="?page=gallery" class="poppins">Gallery</a>
            </li>
            <li>
                <a href="?page=contact" class="poppins">Contact</a>
            </li>
        </ul>
    </nav>
    <div class="content">
        <?php
            if (isset($_GET['page'])) {
                $page = $_GET['page'];

                switch ($page) {
                    case 'home':
                        include 'app/view/home/index.php';
                        break;
                    case 'major':
                        include 'app/view/major/index.php';
                        break;
                    case 'teacher':
                        include 'app/view/heroes/index.php';
                        break;
                    case 'interpersonal':
                        include 'app/view/interpersonal/index.php';
                        break;
                    case 'facility':
                        include 'app/view/facility/index.php';
                        break;
                    case 'gallery':
                        include 'app/view/gallery/index.php';
                        break;
                    case 'contact':
                        include 'app/view/contact/index.php';
                        break;
                    default:
                        echo "<div class='row poppins text-matt p-32'>
                                <div class='text-center' style='padding-top: 0px;'>
                                    <center>
                                        <h2>Ooooopsss</h2>
                                        <p class='p-8' style='font-size: 18px;'>“Errors 404 Not Found”</p>
                                    </center>
                                </div>
                            </div>";
                        break;
                }
            } else {
                include 'app/view/home/index.php';
            }
        ?>
    </div>

    <!-- primary js -->
    <script src="assets/styles/javascripts/application.js"></script>
</body>
</html>